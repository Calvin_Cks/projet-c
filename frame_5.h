#ifndef FRAME_5_H_INCLUDED
#define FRAME_5_H_INCLUDED

#include <wx/wx.h>
enum
{
    ID_UPD4,
    ID_3,
    ID_4,
    ID_L,
    ID_Q,
    ID_S,
    ID_2
};

class Frame_5: public wxFrame
{
public:
    Frame_5(wxWindow* parent, const wxString x);
    virtual ~Frame_5();
    void historique();
    void close(wxCommandEvent &event);
private:
    wxTextCtrl *txt_1, *txt_2,*txt1,*txt2;
    wxButton *mod3,*mod4,*quit_2;

};
#endif // FRAME_5_H_INCLUDED
