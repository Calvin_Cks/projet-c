#include "appli.h"

IMPLEMENT_APP(MyApp);

#include "main.h"
#include "frame.h"
#include "frame_1.h"
#include "frame_2.h"
#include "frame_3.h"
#include "frame_4.h"
#include "frame_5.h"
#include <wx/statline.h>
#include <wx/radiobut.h>
#include <iostream>
#include <cstdlib>
#include <fstream>

using namespace std;

MainFrame::MainFrame(wxWindow* parent, const wxString x): wxFrame(parent,-1,x)
{
  wxBoxSizer *sizer_1 = new wxBoxSizer(wxVERTICAL);

       wxPanel *zone_0= new wxPanel(this,-1);
       wxStaticBoxSizer *cadre =new wxStaticBoxSizer(wxHORIZONTAL,zone_0,(""));
       zone_0->SetSizer(cadre);
       sizer_1->Add(zone_0,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_int = new wxBoxSizer(wxVERTICAL);
       wxFlexGridSizer *grille = new wxFlexGridSizer(3,2,5,5);

       wxStaticText *label1 = new wxStaticText(zone_0,-1,("Nom:"));
       grille->Add(label1,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt1=new wxTextCtrl(zone_0,ID_NOM,_T(""));
       grille->Add(txt1,0,wxEXPAND);

       wxStaticText *label2 = new wxStaticText(zone_0,-1,("Pr�nom:"));
       grille->Add(label2,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt2=new wxTextCtrl(zone_0,ID_PRENOM,_T(""));
       grille->Add(txt2,0,wxEXPAND);

       wxStaticText *label3 = new wxStaticText(zone_0,-1,("Identifiant:"));
       grille->Add(label3,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt3=new wxTextCtrl(zone_0,ID_ID,_T(""));
       grille->Add(txt3,0,wxEXPAND);

       cadre->Add(grille,1,wxALL|wxEXPAND,5);
       grille->AddGrowableCol(1);


       wxBoxSizer *sizer_bt = new wxBoxSizer(wxHORIZONTAL);
       connexion = new wxButton (zone_0,ID_GER,("CONNEXION"),wxPoint(150,160),wxSize(80,20));

       sizer_int->Add(sizer_bt,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer_1);

       Connect(ID_GER,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(MainFrame::Onconnexion));

}

Frame::Frame(wxWindow* parent, const wxString x):wxFrame(parent,-1,x)
{
    wxBoxSizer *sizer_1 = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau_2= new wxPanel(this,-1);
       wxStaticBoxSizer *carre_2 =new wxStaticBoxSizer(wxHORIZONTAL,panneau_2,(""));
       panneau_2->SetSizer(carre_2);
       sizer_1->Add(panneau_2,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_con = new wxBoxSizer(wxVERTICAL);

       //sizer_con->Add(carre_2,1,wxALL | wxEXPAND,5);

       wxBoxSizer *sizer_bn = new wxBoxSizer(wxHORIZONTAL);
       connexe = new wxButton (panneau_2,ID_CONNEXE,("OK"),wxPoint(150,160),wxSize(80,20));

       //sizer_bn->Add(panneau_2,0,wxALIGN_CENTER_VERTICAL|wxALL,5);

       wxBoxSizer*checking_1 = new wxBoxSizer(wxVERTICAL);
       check_1 = new wxRadioButton(panneau_2,wxID_ANY,("Maintenance"),wxPoint(20,50));
       check_2 = new wxRadioButton(panneau_2,wxID_ANY,("Acc�der � la salle"),wxPoint(250,50));
       checking_1->Add(check_1,1);
       checking_1->Add(check_2,1);

       sizer_con->Add(sizer_bn,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer_1);


   Connect(ID_CONNEXE,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame::OnOk));
   Connect(ID_CHECK_1,ID_CHECK_2,wxEVT_COMMAND_RADIOBUTTON_SELECTED,wxCommandEventHandler(Frame::OnOk));

}
Frame::Frame(const wxString& titre):wxFrame(NULL,wxID_ANY, titre)
{
    wxBoxSizer *sizer_t = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau_3= new wxPanel(this,-1);
       wxStaticBoxSizer *carre_3 =new wxStaticBoxSizer(wxHORIZONTAL,panneau_3,(""));
       panneau_3->SetSizer(carre_3);
       sizer_t->Add(panneau_3,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_ter = new wxBoxSizer(wxVERTICAL);

       wxBoxSizer *sizer_bt = new wxBoxSizer(wxHORIZONTAL);
       ok = new wxButton (panneau_3,ID_OK,("OK"),wxPoint(130,160),wxSize(120,20));


       wxBoxSizer *checking_2 = new wxBoxSizer(wxVERTICAL);
       check_3 = new wxRadioButton(panneau_3,wxID_ANY,("Enregistrer"),wxPoint(20,50));
       check_5 =new wxRadioButton(panneau_3,wxID_ANY,("D�finition d'horaire"),wxPoint(140,100));
       check_6 = new wxRadioButton(panneau_3,wxID_ANY,("Historique"),wxPoint(280,50));
       checking_2->Add(check_3,1);
       checking_2->Add(check_5,1);
       checking_2->Add(check_6,1);

       sizer_bt->Add(checking_2,0,wxALL,5);

       sizer_ter->Add(sizer_bt,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer_t);
       Connect(ID_OK,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame::Onok_1));
       Connect(ID_CHECK_3,ID_CHECK_4,wxEVT_COMMAND_RADIOBUTTON_SELECTED,wxCommandEventHandler(Frame::Onok_1));
       Connect(ID_CHECK_6,wxEVT_COMMAND_RADIOBUTTON_SELECTED,wxCommandEventHandler(Frame::Onok_1));
}

MainFrame::MainFrame(const wxString& title):wxFrame(NULL,wxID_ANY, title, wxDefaultPosition,wxSize(320,260),wxSYSTEM_MENU|wxCAPTION|wxCLOSE_BOX|wxCLIP_CHILDREN)
{
   wxBoxSizer *sizer_vertical = new wxBoxSizer(wxVERTICAL);

       zone1 = new wxPanel(this,-1);
       wxStaticBoxSizer *cadre1 =new wxStaticBoxSizer(wxHORIZONTAL,zone1,("Statut"));
       zone1->SetSizer(cadre1);
       sizer_vertical->Add(zone1,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_inter = new wxBoxSizer(wxVERTICAL);


       wxBoxSizer *sizer_btn = new wxBoxSizer(wxHORIZONTAL);
       valider = new wxButton (zone1,ID_BTN_VALIDER,("VALIDER"),wxPoint(120,160),wxSize(80,20));


       wxBoxSizer*checking = new wxBoxSizer(wxVERTICAL);
       check = new wxRadioButton(zone1,wxID_ANY,("Administration"),wxPoint(20,50));
       check1 = new wxRadioButton(zone1,wxID_ANY,("Etudiant"),wxPoint(220,50));
       checking->Add(check,1);
       checking->Add(check1,1);


       sizer_btn->Add(checking,0,wxALL,5);

       sizer_inter->Add(sizer_btn,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer_vertical);


   Connect(ID_BTN_VALIDER,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(MainFrame::Onvalider));
   Connect(ID_CHECK,ID_CHECK1,wxEVT_COMMAND_RADIOBUTTON_SELECTED,wxCommandEventHandler(MainFrame::Onvalider));

}

Frame_1::Frame_1(const wxString& titre):wxFrame(NULL,wxID_ANY, titre)
{
   wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau= new wxPanel(this,-1);
       wxStaticBoxSizer *carre =new wxStaticBoxSizer(wxHORIZONTAL,panneau,(""));
       panneau->SetSizer(carre);
       sizer->Add(panneau,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_int = new wxBoxSizer(wxVERTICAL);
       wxFlexGridSizer *grille = new wxFlexGridSizer(4,2,5,5);

       wxStaticText *label_4 = new wxStaticText(panneau,-1,("Statut:"));
       grille->Add(label_4,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt6=new wxTextCtrl(panneau,ID_STATUT,_T(""));
       grille->Add(txt6,0,wxEXPAND);

       wxStaticText *label1 = new wxStaticText(panneau,-1,("Nom:"));
       grille->Add(label1,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt1=new wxTextCtrl(panneau,ID_NOM,_T(""));
       grille->Add(txt1,0,wxEXPAND);

       wxStaticText *label2 = new wxStaticText(panneau,-1,("Pr�nom:"));
       grille->Add(label2,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt2=new wxTextCtrl(panneau,ID_PRENOM,_T(""));
       grille->Add(txt2,0,wxEXPAND);

       wxStaticText *label3 = new wxStaticText(panneau,-1,("Identifiant:"));
       grille->Add(label3,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt3=new wxTextCtrl(panneau,ID_ID,_T(""));
       grille->Add(txt3,0,wxEXPAND);

       carre->Add(grille,1,wxALL|wxEXPAND,5);
       grille->AddGrowableCol(1);

       wxBoxSizer *sizer_b = new wxBoxSizer(wxHORIZONTAL);
       reg = new wxButton (panneau,ID_REG,("ENREGISTRER"),wxPoint(150,160),wxSize(80,20));

       sizer_int->Add(sizer_b,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer);

       Connect(ID_REG,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame_1::enregistrer));
}

Frame_1::Frame_1(wxWindow* parent, const wxString x):wxFrame(parent,-1,x)
{
   wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau= new wxPanel(this,-1);
       wxStaticBoxSizer *carre =new wxStaticBoxSizer(wxHORIZONTAL,panneau,(""));
       panneau->SetSizer(carre);
       sizer->Add(panneau,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_int = new wxBoxSizer(wxVERTICAL);
       wxFlexGridSizer *grille = new wxFlexGridSizer(3,2,5,5);

       wxStaticText *label1 = new wxStaticText(panneau,-1,("Nom:"));
       grille->Add(label1,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt1=new wxTextCtrl(panneau,ID_NOM,_T(""));
       grille->Add(txt1,0,wxEXPAND);

       wxStaticText *label2 = new wxStaticText(panneau,-1,("Pr�nom:"));
       grille->Add(label2,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt2=new wxTextCtrl(panneau,ID_PRENOM,_T(""));
       grille->Add(txt2,0,wxEXPAND);

       wxStaticText *label3 = new wxStaticText(panneau,-1,("Identifiant:"));
       grille->Add(label3,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt3=new wxTextCtrl(panneau,ID_ID,_T(""));
       grille->Add(txt3,0,wxEXPAND);

       carre->Add(grille,1,wxALL|wxEXPAND,5);
       grille->AddGrowableCol(1);


       wxBoxSizer *sizer_b = new wxBoxSizer(wxHORIZONTAL);
       ger = new wxButton (panneau,ID_GER,("CONNEXION"),wxPoint(150,160),wxSize(80,20));

       sizer_int->Add(sizer_b,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer);

       Connect(ID_GER,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame_1::Onconnexion_1));
}

Frame_2::Frame_2(wxWindow* parent, const wxString x):wxFrame(parent,-1,x)
{

  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau= new wxPanel(this);
       wxStaticBoxSizer *carre =new wxStaticBoxSizer(wxHORIZONTAL,panneau,(""));
       panneau->SetSizer(carre);
       sizer->Add(panneau,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_1 = new wxBoxSizer(wxVERTICAL);

       wxBoxSizer *sizer_2 = new wxBoxSizer(wxHORIZONTAL);

       wxStaticText *label1 = new wxStaticText(panneau,-1,("Etudiant:"));
       txt_i=new wxTextCtrl(panneau,ID_NOM,_T(""));

       wxStaticText *label2 = new wxStaticText(panneau,-1,("   /"));
       txt_j=new wxTextCtrl(panneau,ID_PRENOM,_T(""));

       sizer_2->Add(label1,1,wxALL|wxALIGN_CENTER,0);
       sizer_2->Add(txt_i,1,wxALL|wxALIGN_CENTER,0);
       sizer_2->Add(label2,1,wxALL|wxALIGN_CENTER,20);
       sizer_2->Add(txt_j,0,wxALL|wxALIGN_CENTER,0);

       sizer_1->Add(sizer_2,1,wxALL,5);


          wxBoxSizer *sizer_3 = new wxBoxSizer(wxHORIZONTAL);

       wxStaticText *label3 = new wxStaticText(panneau,-1,("Administration:"));
       txt_k=new wxTextCtrl(panneau,ID_AD,_T(""));

       wxStaticText *label4 = new wxStaticText(panneau,-1,("   /"));
       txt_l=new wxTextCtrl(panneau,ID_M,_T(""));

       sizer_3->Add(label3,1,wxALL|wxALIGN_CENTER,0);
       sizer_3->Add(txt_k,1,wxALL|wxALIGN_CENTER,0);
       sizer_3->Add(label4,1,wxALL|wxALIGN_CENTER,20);
       sizer_3->Add(txt_l,0,wxALL|wxALIGN_CENTER,0);

      sizer_1->Add(sizer_3,1,wxALL,5);

        wxBoxSizer *sizer_b = new wxBoxSizer(wxHORIZONTAL);
       def = new wxButton (panneau,ID_DEF,("DEFINIR"),wxPoint(150,160),wxSize(80,20));

       sizer_1->Add(sizer_b,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       carre->Add(sizer_1,1,wxALL,0);


  Connect(ID_DEF,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame_2::def_horaire));
}

Frame_5::Frame_5(wxWindow* parent, const wxString x):wxFrame(parent,-1,x)
{
wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);

       wxPanel *panneau= new wxPanel(this,-1);
       wxStaticBoxSizer *carre =new wxStaticBoxSizer(wxHORIZONTAL,panneau,(""));
       panneau->SetSizer(carre);
       sizer->Add(panneau,2,wxALL|wxEXPAND,0);

       wxBoxSizer *sizer_int = new wxBoxSizer(wxVERTICAL);
       wxFlexGridSizer *grille = new wxFlexGridSizer(2,2,5,5);

       wxStaticText *label_4 = new wxStaticText(panneau,-1,("Administration:"));
       grille->Add(label_4,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt_1=new wxTextCtrl(panneau,ID_3,_T(""));
       grille->Add(txt_1,0,wxEXPAND);

       wxStaticText *label1 = new wxStaticText(panneau,-1,("Etudiant:"));
       grille->Add(label1,0,wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
       txt_2=new wxTextCtrl(panneau,ID_4,_T(""));
       grille->Add(txt_2,0,wxEXPAND);

       wxColour *color_2 = new wxColour (0,0,255);
       label_4->SetForegroundColour(*color_2);

       wxColour *color_3 = new wxColour (0,0,255);
       label1->SetForegroundColour(*color_3);

       wxColour *color = new wxColour (255,128,64);
       txt_1->SetForegroundColour(*color);

       wxColour *color_1 = new wxColour (255,128,64);
       txt_2->SetForegroundColour(*color_1);

       carre->Add(grille,1,wxALL|wxEXPAND,5);
       grille->AddGrowableCol(1);

       wxBoxSizer *sizer_b = new wxBoxSizer(wxHORIZONTAL);
       quit_2 = new wxButton (panneau,ID_Q,("QUITTER"),wxPoint(150,160),wxSize(80,20));

       sizer_int->Add(sizer_b,0,wxALIGN_CENTER_HORIZONTAL|wxALL,5);

       SetSizer(sizer);

      Connect(ID_Q,wxEVT_COMMAND_BUTTON_CLICKED,wxCommandEventHandler(Frame_5::close ));
}

Frame::~Frame()
{

}

MainFrame::~MainFrame()
{

}

Frame_1::~Frame_1()
{

}
Frame_2::~Frame_2()
{

}

Frame_3::~Frame_3()
{

}

Frame_4::~Frame_4()
{

}

Frame_5::~Frame_5()
{

}

void MainFrame::Onvalider(wxCommandEvent &event)
{

    bool C,R;
    C = check1->GetValue();
    R = check->GetValue();

    if(C==true)
    {
       const wxString x ("Espace �tudiant");
       MainFrame *frm_1 = new MainFrame(this,x);
       this->Show(false);
       frm_1->Show(TRUE);
    }
    else if(R==true)
    {
        const wxString x ("Espace administration");
        Frame *frm_2 = new Frame(this,x);
        this->Show(false);
        frm_2->Show(TRUE);
    }

}

void MainFrame::Onconnexion(wxCommandEvent &event)
{
    user.Nom = txt1->GetValue().c_str();
    user.Prenom = txt2->GetValue().c_str();
    user.cod = atof(txt3->GetValue().c_str());

    ifstream fichier {"register.dat"};
    ifstream seen {"horaire.dat"};
     int x,y;
    int test=0,test_1=0,i,hour;

       while(!fichier.eof())
    {
        fichier >> utilisateur.Statut >> utilisateur.Nom >> utilisateur.Prenom >> utilisateur.code;
        if(user.Nom==utilisateur.Nom && user.Prenom==utilisateur.Prenom && user.cod==utilisateur.code)
                {
                    test = 1;
                    time_t actuel = time(0);
                    tm *ltm = localtime(&actuel);
                    hour=ltm->tm_hour;

                    seen >> x >> y ;

                     if(hour>x && hour<y)
                    {
                         test_1=1;
                        ifstream search {"historique_2.dat"};
                        search >> i;
                        ofstream cat {"historique_2.dat" , ios::trunc};
                        i+=1;
                        cat<<i;
                    }

                }
    }

            if(test==1 && test_1==1)
            {
                 wxMessageBox(("Acc�s � la salle autoris�e!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);

               /* wxSound snd=new wxSound("son.wav",true);
                snd->Play(wxSOUND_ASYNC | wxSOUND_LOOP);*/

            }
            else if(test==0)
            {
                wxMessageBox(("Informations incorrectes!La porte ne peut �tre ouverte!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);
            }
            else if(test_1==0)
            {
                wxMessageBox(("Heure d'acc�s expir�e!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);
            }
}

void Frame::OnOk(wxCommandEvent &event)
{

    bool D,X;
    D = check_1->GetValue();
    X = check_2->GetValue();

    if(D==true)
    {
       Frame *frame=new Frame("Maintenance");
       this->Show(false);
       frame->Show(TRUE);
    }
    else if(X==true)
    {
        const wxString x ("Acc�s � salle");
        Frame_1 *frm_2 = new Frame_1(this,x);
        this->Show(false);
        frm_2->Show(TRUE);
    }
}

void Frame::Onok_1(wxCommandEvent &event)
{

    bool E,F,H;
    E = check_3->GetValue();
    F = check_5->GetValue();
    H = check_6->GetValue();

     if(E==true)
    {
       Frame_1 *frm_6=new Frame_1("Enregistrement");
       this->Show(false);
       frm_6->Show(TRUE);
    }
    else if(F==true)
    {
        const wxString x ("D�finition d'horaires");
        Frame_2 *frm_7 = new Frame_2(this,x);
        this->Show(false);
        frm_7->Show(TRUE);
    }
   else if(H==true)
    {
      const wxString x ("Historique d'acc�s � la salle");
        Frame_5 *frm_11 = new Frame_5(this,x);
        frm_11->historique();
        this->Show(false);
        frm_11->Show(TRUE);

    }
}

void Frame_1::enregistrer(wxCommandEvent &event)
{
    utilisateur.Statut = txt6->GetValue().c_str();
    utilisateur.Nom = txt1->GetValue().c_str();
    utilisateur.Prenom = txt2->GetValue().c_str();
    utilisateur.code = atof(txt3->GetValue().c_str());

    if (utilisateur.Statut=="Etudiant")
    {
        fstream point ("register.dat" , ios::app);
        point<<utilisateur.Statut<<" ";
        point<<utilisateur.Nom<<" ";
        point<<utilisateur.Prenom<<" ";
        point<<utilisateur.code<<endl;
        wxMessageBox(("Nouvel utilisateur enregistr�!"),("ENREGISTREMENT"),wxOK|wxICON_DEFAULT_TYPE);
    }
    else if (utilisateur.Statut=="Administration")
    {
        fstream fic ("admin.dat" , ios::app);
        fic<<utilisateur.Statut<<" ";
        fic<<utilisateur.Nom<<" ";
        fic<<utilisateur.Prenom<<" ";
        fic<<utilisateur.code<<endl;
        wxMessageBox(("Nouvel utilisateur enregistr�!"),("ENREGISTREMENT"),wxOK|wxICON_DEFAULT_TYPE);
    }
    else
        {
            wxMessageBox(("Entrez un statut valide!"),("ENREGISTREMENT"),wxOK|wxICON_DEFAULT_TYPE);
        }
}

void Frame_1::Onconnexion_1(wxCommandEvent &event)
{
    user.Nom = txt1->GetValue().c_str();
    user.Prenom = txt2->GetValue().c_str();
    user.cod = atof(txt3->GetValue().c_str());
        ifstream fic {"admin.dat"};
        ifstream saw {"horaire_ad.dat"};
        int t,z;
          int test=0,test_1=0,i,heure;

       while(!fic.eof())
    {
        fic >> utilisateur.Statut >> utilisateur.Nom >> utilisateur.Prenom >> utilisateur.code;
        if(user.Nom==utilisateur.Nom && user.Prenom==utilisateur.Prenom && user.cod==utilisateur.code)
                {
                    test=1;
                    time_t actuel = time(0);
                    tm *ltm = localtime(&actuel);
                    heure=ltm->tm_hour;

                    saw >> t >> z;

                     if(heure>t && heure<z)
                    {
                        test_1=1;
                        ifstream search {"historique.dat"};
                        search >> i;
                        ofstream cat {"historique.dat" , ios::trunc};
                        i+=1;
                        cat<<i;
                    }

                }
             }
                if(test==1 && test_1==1)
                    {
                        wxMessageBox(("Acc�s � la salle autoris�e!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);
                    }
                    else if(test==0)
                    {
                        wxMessageBox(("Informations incorrectes!La porte ne peut �tre ouverte!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);
                    }
                    else if(test_1==0)
                    {
                        wxMessageBox(("Heure d'acc�s expir�e!"),("Acc�s � la salle"),wxOK|wxICON_DEFAULT_TYPE);
                    }
}


void Frame_5::historique()
{
   ifstream search {"historique.dat"};
     int i;
      search >> i;
       (*txt_1)<< i <<" (fois)";

  ifstream see {"historique_2.dat"};
   int j;
    see >> j;
      (*txt_2)<< j <<" (fois)";
}
void Frame_5::close(wxCommandEvent &event)
{
  this->Destroy();
}

void Frame_2::def_horaire(wxCommandEvent &event)
{
    int i,j,k,l;
    i = atof(txt_i->GetValue().c_str());
    j = atof(txt_j->GetValue().c_str());
    k = atof(txt_k->GetValue().c_str());
    l = atof(txt_l->GetValue().c_str());

        ofstream point ("horaire.dat" , ios::trunc);
        point<< i <<" "<< j << endl;

        ofstream fic ("horaire_ad.dat" , ios::trunc);
        fic<< k <<" "<< l << endl;

        wxMessageBox(("Cr�neau horaire d�fini!"),("D�finition d'horaire"),wxOK|wxICON_DEFAULT_TYPE);
}

bool MyApp::OnInit()
{
    MainFrame *frame=new MainFrame("ACCEUIL");
    frame->Show(TRUE);
    SetTopWindow(frame);
    return true;
}





