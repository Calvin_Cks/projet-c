#ifndef FRAME_2_H_INCLUDED
#define FRAME_2_H_INCLUDED

#include <wx/wx.h>
enum
{
    ID_OKAY,
    ID_CHECK_7,
    ID_CHECK_8,
    ID_CHECK_9,
    ID_CHECK_10,
    ID_CHECK_11,
    ID_CHECK_12,
    ID_AD,
    ID_H,
    ID_M,
    ID_OKAY1,
    ID_DEF
};

class Frame_2: public wxFrame
{
public:
    Frame_2(wxWindow* parent, const wxString x);
    Frame_2(const wxString& titre);
    virtual ~Frame_2();
    void def_horaire(wxCommandEvent &event);

private:
    wxButton *okay,*okay_1,*def;
    wxRadioButton *check_7,*check_8,*check_9,*check_10,*check_11,*check_12;
    wxTextCtrl *txt_i,*txt_j,*txt_k,*txt_l;
};
#endif // FRAME_2_H_INCLUDED

