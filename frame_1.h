#ifndef FRAME_1_H_INCLUDED
#define FRAME_1_H_INCLUDED

#include <wx/wx.h>
enum
{
    ID_REG,
    ID_GER
};

typedef struct add_1 add_1;
typedef struct chec_1 chec_1;

struct add_1
{
      std::string Nom;
      std::string Prenom;
      std::string Statut;
      long code;
};

struct chec_1
{
    std::string Nom;
    std::string Prenom;
    long cod;
};

class Frame_1: public wxFrame
{
public:
    Frame_1(wxWindow* parent, const wxString x);
    Frame_1(const wxString& titre);
    virtual ~Frame_1();
    void enregistrer(wxCommandEvent &event);
    void Onconnexion_1(wxCommandEvent &event);
private:
    wxTextCtrl *txt1, *txt2, *txt3, *txt4, *txt5 , *txt6;
    wxButton *reg,*ger;
    add_1 utilisateur;
    chec_1 user;

};
#endif // FRAME_1_H_INCLUDED
