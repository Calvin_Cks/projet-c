#ifndef FRAME_4_H_INCLUDED
#define FRAME_4_H_INCLUDED

#include <wx/wx.h>
enum
{
    ID_UPD3,
    ID_STA,
    ID_1,
    ID_K

};


class Frame_4: public wxFrame
{
public:
    Frame_4(const wxString& title);
    virtual ~Frame_4();
private:
    wxTextCtrl *txt1, *txt2;
    wxButton *mod3,*mod4,*okay_2;
    wxRadioButton *check_11,*check_12;

};
#endif // FRAME_4_H_INCLUDED
