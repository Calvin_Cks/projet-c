#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <wx/wx.h>

enum
{
    ID_BTN_VALIDER=wxID_HIGHEST+1,
    ID_CONNEXION,
    ID_NOM,
    ID_PRENOM,
    ID_ID,
    ID_CHECK,
    ID_CHECK1,
    ID_STATUT,
    ID_BIT
};
typedef struct add add;
typedef struct chec chec;

struct add
{
      std::string Nom;
      std::string Prenom;
      std::string Statut;
      long code;
};

struct chec
{
    std::string Nom;
    std::string Prenom;
    long cod;
};

class MainFrame: public wxFrame
{
public:
    MainFrame(wxWindow* parent, const wxString x);
    MainFrame(const wxString& title);
    virtual ~MainFrame();
    void Onvalider(wxCommandEvent &event);
    void Onconnexion(wxCommandEvent &event);
    void OnCheck(wxCommandEvent &event);

private:
    wxPanel *zone1;
    wxTextCtrl *txt1, *txt2, *txt3, *txt4, *txt5,*txt6;
    wxButton *valider,*connexion;
    wxRadioButton *check,*check1;
     add utilisateur;
     chec user;
};


#endif // MAIN_H_INCLUDED
