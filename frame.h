#ifndef FRAME_H_INCLUDED
#define FRAME_H_INCLUDED

#include <wx/wx.h>
enum
{
    //ID_BTN_VALIDER=wxID_HIGHEST+1,
    ID_CONNEXE,
    ID_NAME,
    ID_PRENAME,
    ID_IDE,
    ID_ENR,
    ID_MOD,
    ID_HOR,
    ID_HIST,
    ID_CHECK_1,
    ID_CHECK_2,
    ID_CHECK_3,
    ID_CHECK_4,
    ID_CHECK_5,
    ID_CHECK_6,
    ID_OK

};

class Frame: public wxFrame
{
public:
    Frame(wxWindow* parent, const wxString x);
    Frame(const wxString& titre);
    virtual ~Frame();
    void OnOk(wxCommandEvent &event);
    void Onok_1(wxCommandEvent &event);

private:
    wxTextCtrl *txt1, *txt2, *txt3, *txt4, *txt5;
    wxButton *connexe,*ok;
    wxRadioButton *check_1,*check_2;
    wxRadioButton *check_3,*check_4,*check_5,*check_6;

};
#endif // FRAME_H_INCLUDED
