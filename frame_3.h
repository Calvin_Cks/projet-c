
#ifndef FRAME_3_H_INCLUDED
#define FRAME_3_H_INCLUDED

#include <wx/wx.h>
enum
{
    ID_UPD1,
    ID_UPD2,
    ID_STAT,
    ID_I

};


class Frame_3: public wxFrame
{
public:
    Frame_3(wxWindow* parent, const wxString x);
    Frame_3(const wxString& title);
    virtual ~Frame_3();

private:
    wxTextCtrl *txt1, *txt2;
    wxButton *mod1,*mod2;
    wxCheckBox *check_7,*check_8,*check_9,*check_10,*check_11;


};
#endif // FRAME_3_H_INCLUDED
